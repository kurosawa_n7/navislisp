
module Main where

import NavisLisp(runRepl)


main :: IO ()
--main = do
--  putStrLn message

main = runRepl >> return ()
