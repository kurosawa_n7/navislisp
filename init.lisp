;;;; Initialization

(defmacro let* (bind :rest forms)
  (%expand-let* bind forms))

(defun %expand-let* (bind forms)
  (if (null bind)
      `(progn ,@forms)
    `(let (,(car bind)) ,(%expand-let* (cdr bind) forms))))

(defun eq (o1 o2)
  (eql o1 o2))

(defun not (p)
  (if p nil t))

(defmacro and (:rest forms) (%expand-and forms))

(defmacro or (:rest forms) (%expand-or forms))

(defun %expand-and (form)
  (if (null form) 't
    `(if ,(car form) ,(%expand-and (cdr form)) nil)))

(defun %expand-or (form)
  (if (null form) 'nil
    `(if ,(car form) t ,(%expand-or (cdr form)))))

(defmacro cond (:rest testforms)
  (%expand-cond testforms))

(defun %expand-cond (testforms)
  (if (null testforms)
      nil
    (%expand-cond-1 (car testforms) (%expand-cond (cdr testforms)))))

(defun %expand-cond-1 (testform next)
  (if (null testform)
      next
    (let ((test (car testform))
         (form (cdr testform)))
      (if (null form)
        `(or ,test ,next)
        `(if ,test (progn ,@form) ,next)))))


(defmacro case (keyform :rest forms)
  (%expand-case '#'eql keyform forms))

(defmacro case-using (predform keyform :rest forms)
  (%expand-case predform keyform forms))

(defun %expand-case (predform keyform forms)
  (let ((g (gensym)))
    `(let ((,g ,keyform))
       ,(%expand-case-testform predform g forms))))

(defun %expand-case-testform (predform g testforms)
  (if (null testforms)
      'nil
    (let ((current (car testforms))
          (next (cdr testforms)))
      (if (null current)
        (%expand-case-testform predform g next)
        (let ((key (car current))
             (form (cdr current)))
          (if (eql key 't)
            (if (null next)
              `(progn ,@form)
;               (error "syntax error `case'"))
              nil)
            (if (listp key)
              `(if (%member-using ,predform ,g (quote ,key))
                  (progn ,@form)
                  ,(%expand-case-testform predform g next))
;             (error "syntax error `case'"))))))))
              nil)))))))

(defmacro while (test :rest body)
  (let ((tag (gensym)))
    `(tagbody
       ,tag
       (if ,test
         (progn ,@body (go ,tag))))))

(defmacro for (iterspec endresult :rest form)
  (%expand-for iterspec endresult form))

(defun %expand-for (iterspec endresult form)
  (if (not (%expand-for-check-specs iterspec '()))
      (error "expanding for"))
  (if (consp endresult)
      (if (consp (cdr endresult))
        (%expand-for-loop iterspec (car endresult) (cdr endresult) form)
        (%expand-for-loop iterspec (car endresult) nil form))
    (%expand-for-loop iterspec endresult nil form)))

(defun %expand-for-check-specs (specs acc)
  (if (null specs)
      t
    (let* ((current (car specs))
           (next (cdr specs)))
      (and (listp current)
           (let ((clen (length current)))
             (or (= clen 2) (= clen 3)))
           (symbolp (car current))
           (not (member (car current) acc))
           (%expand-for-check-specs next (cons (car current) acc))))))

(defun %expand-for-loop (iterspec endtest result form)
  (labels ((make-step (spec)
             (let* ((var (car spec))
                    (step var))
               (if (cdr (cdr spec)) (setq step (car (cdr (cdr spec)))))
               `(setq ,var ,step)))
           (make-init (spec)
             (let ((var (car spec))
                   (init (car (cdr spec))))
             `(,var ,init))))
    (let ((steps (mapcar #'make-step iterspec))
          (inits (mapcar #'make-init iterspec)))
      `(let ,inits
         (while (not ,endtest) ,@form ,@steps)
         ,@result))))

(defun instancep (obj cls)
  (eq (class-of obj) cls))

(defmacro assure (cname form)
  (let ((g (gensym)))
    `(let ((,g ,form))
       (if (eq (class ,cname) (class-of ,g))
           ,g
         (error "domain-error at `assure'")))))

(defmacro the (cname form)
  (assure cname form))

(defun >= (x y)
  (not (< x y)))

(defun <= (x y)
  (not (> x y)))

(defun reciprocal (x)
  (/ 1.0 x))

(defun max (x :rest xs)
  (if (null xs)
      x
    (let ((%max (apply #'max xs)))
      (if (< x %max)
          %max
        x))))

(defun min (x :rest xs)
  (if (null xs)
      x
    (let ((%min (apply #'min xs)))
      (if (< x %min)
          x
        %min))))

(defun abs (x)
  (if (> x 0)
      x
    (- x)))

(defglobal *pi* 3.141592653589793238462)

(defun null (p) (not p))

(defun listp (l)
  (or (consp l) (null l)))

(defun create-list (i :rest element)
  (if (null element)
      (create-list i nil)
    (if (cdr element)
;        (error "arity error")
        nil
      (%create-list i (car element)))))

(defun %create-list (i element)
  (if (== 0 i)
      nil
    (cons i (%create-list (- i 1) element))))


(defun reverse (list)
  (%reverse-tail list nil))

(defun %reverse-tail (list acc)
  (if (consp list)
      (%reverse-tail (cdr list) (cons (car list) acc))
    acc))

(defun nreverse (list)
  (reverse list))

(defun member (o list)
  (%member-using #'eql o list))

(defun %member-using (fn o list)
  (if (consp list)
      (or (funcall fn o (car list)) (%member-using fn o (cdr list)))
    nil))


(defun %collect-car/cdr (list :rest lists)
  (labels ((repeat (ls car/ cdr/)
             (if (not (consp ls))
                 (cons (reverse car/) (reverse cdr/))
               (let ((head (car ls))
                    (tail (cdr ls)))
                (if (consp head)
                    (repeat tail (cons (car head) car/) (cons (cdr head) cdr/))
                  nil)))))
    (repeat (cons list lists) '() '())))

(defun mapcar (fn list :rest lists)
  (let ((car/cdr (apply #'%collect-car/cdr list lists)))
    (if (null car/cdr)
        nil
      (cons (apply fn (car car/cdr))
            (apply #'mapcar fn (cdr car/cdr))))))

(defun mapc (fn list :rest lists)
  (apply #'mapcar fn list lists)
  list)

(defun maplist (fn list :rest lists)
  (let ((car/cdr (apply #'%collect-car/cdr list lists)))
    (if (null car/cdr)
        nil
      (cons (apply fn list lists)
            (apply #'maplist fn (cdr car/cdr))))))

(defun mapl (fn list :rest lists)
  (apply #'maplist fn list lists)
  list)

(defun mapcan (fn list :rest lists)
  (apply #'apppend (apply #'mapcar fn (cons list lists))))

(defun mapcon (fn list :rest lists)
  (apply #'append (apply #'maplist fn (cons list lists))))



(defun assoc (obj alist)
  (if (consp alist)
      (if (eql (car (car alist)) obj)
        (car alist)
        (assoc obj (cdr alist)))
      nil))



(defun error (error-string :rest objs)
  (signal-condition
   (create (class <simple-error>)
           'format-string error-string
           'format-arguments objs)
   nil))

(defun cerror (continue-string error-string :rest objs)
  (signal-condition
   (create (class <simple-error>)
           'format-string error-string
           'format-arguments objs)
   (let ((str (create-string-output-stream)))
     (apply #'format str continue-string obj)
     (get-output-stream-string str))))



