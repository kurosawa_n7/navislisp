{-# LANGUAGE TemplateHaskell #-}

module CompileTimeReadFile(compileTimeReadFile) where

import Language.Haskell.TH(Exp, Q, runIO)
import Language.Haskell.TH.Quote(QuasiQuoter(QuasiQuoter, quoteExp))
import Language.Haskell.TH.Syntax(Lift)

compileTimeReadFile :: FilePath -> Q Exp
compileTimeReadFile path = do
  str <- runIO $ readFile path
  [e| str |]

