{-# LANGUAGE GeneralizedNewtypeDeriving, FlexibleContexts, TemplateHaskell #-}
module NavisLisp where

import Control.Monad
import Control.Monad.Trans
import Control.Monad.Reader
import Control.Monad.Except

import Data.Array
import Data.Array.MArray
import Data.Array.IO
import Data.Char
import Data.Maybe
import qualified Data.Map.Strict as M
import qualified Data.IORef as IORef
import Data.Unique
import Data.List

import System.IO
import qualified System.IO.Unsafe as Unsafe

import Debug.Trace

import CompileTimeReadFile(compileTimeReadFile)

-- ----------------------------------------------------------------------------
-- Utilities
-- ----------------------------------------------------------------------------
removeLeftBy :: (a -> a -> Bool) -> [a] -> [a]
removeLeftBy fn [] = []
removeLeftBy fn (x:xs)
    | any (fn x) xs = removeLeftBy fn xs
    | otherwise    = x:removeLeftBy fn xs

removeRightBy :: (a -> a -> Bool) -> [a] -> [a]
removeRightBy fn [] = []
removeRightBy fn (x:xs) = x:removeRightBy fn (filter (not . fn x) xs)

foldrM :: Monad m => (a -> b -> m b) -> b -> [a] -> m b
foldrM f acc [] = return acc
foldrM f acc (x:xs) = foldrM f acc xs >>= f x

unJust = maybe (error "unJust") id

-- ----------------------------------------------------------------------------
-- Basic Object
-- ----------------------------------------------------------------------------

data Symbol = Named   String
            | Unnamed !Unique
    deriving (Eq, Ord)

toSym :: String -> Symbol
toSym = Named . map toLower

genSym :: Eval Symbol
genSym = liftM Unnamed $ liftIOtoEval newUnique

instance Show Symbol where
    show (Named s) = s
    show (Unnamed u) = "#g" ++ show (hashUnique u)

data Ref a = IRef { fromRef :: IORef.IORef a } -- immutable reference
           | MRef { fromRef :: IORef.IORef a } -- mutable reference
           deriving (Eq)

readRef r    = liftIOtoEval $ IORef.readIORef $ fromRef r
writeRef r a = modifyRef r (const a)
modifyRef (IRef r) f = sigError "attempt modifying an immutable object"
modifyRef (MRef r) f = liftIOtoEval $ IORef.modifyIORef r f
newIRef a    = liftM IRef $ liftIOtoEval $ IORef.newIORef a
newMRef a    = liftM MRef $ liftIOtoEval $ IORef.newIORef a


type Dictionary a    = M.Map Symbol a
type DictionaryRef a = Ref (Dictionary a)

addDictionary :: Symbol -> a -> Dictionary a -> Dictionary a
setDictionary :: Symbol -> a -> Dictionary a -> Dictionary a

emptyDictionary = M.empty
addDictionary = M.insert
setDictionary k obj = M.adjust (const obj) k
adjustDictionary = M.adjust
lookupDictionary = M.lookup
elemDictionary k = M.member k
listToDictionary = M.fromList
listFromDictionary = M.toList

type LPair = (LObj, LObj)
type LFun = [LObj] -> Eval LObj
type LMethod = ([Symbol], LFun)
type LambdaList = ([Symbol], Maybe Symbol)
type LStringStream = (Bool, Bool, String)

data LObj = LRef  LClass (DictionaryRef LObj)
          | LCons (Ref LPair)
          | LClsr LOp
          | LMeta LClass
          | LInt  !Integer
          | LFlt  !Double
          | LChar !Char
          | LSym  Symbol
          | LArr  (Ref LArray)
          | LStr  (Ref LArray)
          | LIOS  Handle
          | LStrS (Ref LStringStream)
          | LUnbound

data LArray = LArray{ laDim::[Int], laRef::IOArray Int LObj }

data LOp = LLambda  Symbol LFun
         | LSpecial LFun
         | LGeneric{ lgArity::(Int, Bool), lgPrimary::[LMethod] }

data LClass = LClass
    { lcName  :: Symbol
    , lcSuper :: [Symbol]
    , lcSlots :: [(Symbol, Maybe Symbol, Maybe LObj)]
    , lcAccessor  :: LAccessor
    , lcAbstractp :: Bool
    , lcMetaClass :: Symbol
    }

type LAccessor = Dictionary (Dictionary Symbol)

instance Eq LObj where
    (==) (LRef c1 r1)  (LRef c2 r2)  = c1==c2 && r1==r2
    (==) (LCons r1) (LCons r2) = r1 == r2
    (==) (LClsr (LLambda s1 _)) (LClsr (LLambda s2 _))  = s1 == s2
    (==) (LInt i1)  (LInt i2)  = i1 == i2
    (==) (LFlt f1)  (LFlt f2)  = f1 == f2
    (==) (LSym s1)  (LSym s2)  = s1 == s2
    (==) (LChar c1) (LChar c2) = c1 == c2
    (==) (LArr a1)  (LArr a2)  = a1 == a2
    (==) (LStr s1)  (LStr s2)  = s1 == s2
    (==) (LIOS h1)  (LIOS h2)  = h1 == h2
    (==) (LStrS r1) (LStrS r2) = r1 == r2
    (==) LUnbound   LUnbound   = True
    (==) _ _ = False

instance Eq LClass where
    (==) c1 c2  = lcName c1 == lcName c2

nilSym = LSym (toSym "nil")
tSym = LSym (toSym "t")

isLSym (LSym _) = True
isLSym _        = False

isLInt (LInt _) = True
isLInt _ = False

toLBool p = if p then tSym else nilSym

setSlot :: Symbol -> LObj -> LObj -> Eval ()
setSlot s v (LRef c r) = do
    dic <- readRef r
    case lookupDictionary s dic of
      Just _   -> modifyRef r (setDictionary s v)
      Nothing  -> sigError $ "slot access error"
setSlot _ _ _ = sigError $ "slot access error"

getSlot :: Symbol -> LObj -> Eval LObj
getSlot s (LRef c r) = do
    dic <- readRef r
    case lookupDictionary s dic of
      Just v  -> return v
      Nothing -> sigError $ "slot access error"
getSlot _ _ = sigError $ "slot access error"

-- ----------------------------------------------------------------------------
-- Environment
-- ----------------------------------------------------------------------------

data Env = Env { lexicalVar :: [DictionaryRef LObj]
               , lexicalFun :: [DictionaryRef LOp]
               , classDef :: DictionaryRef LClass
               , setfDef :: DictionaryRef LOp
               , blockName :: Dictionary  (LObj -> Eval LObj)
               , tagbodyTag :: Dictionary (Eval ())
               , dynamicVar :: DictionaryRef (Ref LObj)
               , errHandler :: Ref [LObj -> Eval LObj]
               }

data Signal = Sig_Error String
            | Sig_Conti LObj
            | Sig_Throw LObj LObj
            | Sig_TagGo Symbol [LObj]

instance Show Signal where
    show (Sig_Error s) = "Error:"++s
    show (Sig_Throw _ _) = "non-local exits"
    show (Sig_TagGo _ _) = "lexical control transfer"

-- type Eval a = ReaderT Env (ExceptT Signal IO) a

newtype EvalT m a = EvalT { runEvalT :: ReaderT Env (ExceptT Signal m) a }
    deriving (Functor, Applicative, Monad, MonadReader Env, MonadError Signal, MonadIO)

instance MonadTrans EvalT where
    lift = EvalT . lift . lift

type Eval a = EvalT IO a


liftIOtoEval :: IO a -> Eval a
throwSignal :: Signal -> Eval a
catchSignal :: Eval a -> (Signal -> Eval a) -> Eval a

--liftIOtoEval = lift . liftIO
liftIOtoEval = liftIO
throwSignal = throwError
catchSignal = catchError
sigError = throwSignal . Sig_Error

unwindProtect :: Eval a -> Eval b -> Eval a
unwindProtect x cleanup = do
    r <- x `catchSignal` (\e -> cleanup >> throwSignal e)
    cleanup
    return r

sigCondition o = do
    ref <- askEnv errHandler
    hlist <- readRef ref
    case hlist of
      []     -> sigError "no error handlers"
      (h:hs) -> do
          writeRef ref hs
          unwindProtect ((h o >> sigError "") `catchSignal` catcher)
                        (writeRef ref hlist)
    where
        catcher e@(Sig_Conti v) = return v
        catcher e = throwSignal e


--runEval :: Eval a -> IO (Either Signal a)
runEval m = runExceptT $ runReaderT (runEvalT m') undefined where
    m' = do
        globalVar <- newMRef emptyDictionary
        globalFun <- newMRef emptyDictionary
        defclass <- newMRef emptyDictionary
        defsetf <- newMRef emptyDictionary
        let blocks = emptyDictionary
            tags = emptyDictionary
        dynamic <- newMRef emptyDictionary
        handler <- newMRef []
        local (const (Env [globalVar] [globalFun] defclass defsetf blocks tags dynamic handler)) m

askEnv :: (Env -> a) -> Eval a
askEnv f = liftM f ask

-- lexical variable & function

addGlobalFun = addGlobalBind lexicalFun
addGlobalVar = addGlobalBind lexicalVar
addGeneric = addGlobalFun

addGlobalBind getter s o = do
    bind <- askEnv getter
    modifyRef (last bind) (addDictionary s o)

addClassDef s c = do
    bind <- askEnv classDef
    modifyRef bind (addDictionary s c)

updateLexicalFun = updateLexicalBind lexicalFun
updateLexicalVar = updateLexicalBind lexicalVar

updateLexicalBind getter s o = do
    bind <- askEnv getter
    walkLexical s setter (sigError $ "undeclared symbol "++show s) bind
    where
        setter r _ = modifyRef r (setDictionary s o)

lookupLexicalFun = lookupLexicalBind lexicalFun
lookupLexicalVar = lookupLexicalBind lexicalVar

lookupLexicalBind getter s = do
    bind <- askEnv getter
    walkLexical s (\_ r -> return (Just r)) (return Nothing) bind

lookupClass s = do
    dic <- askEnv classDef >>= readRef
    return $ lookupDictionary s dic

findClass s = lookupClass s >>= maybe (sigError "find class") return

walkLexical :: Symbol -> (DictionaryRef a -> a -> Eval b) -> Eval b
            -> [DictionaryRef a] -> Eval b

walkLexical s succeed failure [] = failure
walkLexical s succeed failure (ref:rs) = do
    dic <- readRef ref
    case lookupDictionary s dic of
      Just r  -> succeed ref r
      Nothing -> walkLexical s succeed failure rs

localFuns = localFrame (\nf e -> e{ lexicalFun = nf:lexicalFun e })
localVars = localFrame (\nf e -> e{ lexicalVar = nf:lexicalVar e })

localFrame modify alist next = do
    nf <- newMRef $ listToDictionary alist
    local (modify nf) next

-- dynamic variable

addDynamicVar s o = do
    bind <- askEnv dynamicVar
    dyn <- newMRef o
    modifyRef bind (addDictionary s dyn)

updateDynamicVar s o = do
    dic <- askEnv dynamicVar >>= readRef
    case lookupDictionary s dic of
      Just ref -> modifyRef ref (const o)
      Nothing  -> sigError $ "no dynamic variable "++show s

findDynamicVar s = do
    d <- lookupDynamicVar s
    case d of
      Just o  -> return o
      Nothing -> sigError $ "no dynamic variable "++show s

lookupDynamicVar :: Symbol -> Eval (Maybe LObj)
lookupDynamicVar s = do
    dic <- askEnv dynamicVar >>= readRef
    case lookupDictionary s dic of
      Nothing  -> return Nothing
      Just ref -> liftM Just $ readRef ref

localDynamicVar :: [(Symbol, LObj)] -> Eval a -> Eval a
localDynamicVar ss body = do
    bind <- askEnv dynamicVar
    oldDic <- readRef bind
    mapM_ (\ (s, o) -> addDynamicVar s o) ss
    unwindProtect body $ modifyRef bind (const oldDic)


-- nonlocal exits
localBlock name fn next = do
    local (\e -> e{ blockName = addDictionary name fn (blockName e) }) next

getBlockExit name = do
    dic <- askEnv blockName
    return $ lookupDictionary name dic

localTagbody tags next = do
    tt <- askEnv tagbodyTag
    let newtags = foldl (\ dic (t, fn) -> addDictionary t fn dic) tt tags
    local (\e -> e{ tagbodyTag = newtags }) next

getTagbodyGo t = do
    tdic <- askEnv tagbodyTag
    return $ lookupDictionary t tdic

-- isToplevel = liftM null (askEnv callStack)

-- generic function
lookupGeneric s = do
    bind <- askEnv lexicalFun
    gdic <- readRef (last bind)
    return $ lookupDictionary s gdic

addMethod s m = do
    liftIOtoEval $ putStrLn $ show $ fst m
    bind <- askEnv lexicalFun
    gdic <- readRef (last bind)
    case lookupDictionary s gdic of
      Just (LGeneric p ms) -> modifyRef (last bind) (setDictionary s (LGeneric p (m:ms)))
      _                    -> sigError $ "no generic function"++show s

findSetf s = do
    setf <- lookupSetf s
    case setf of
      Just r  -> return r
      Nothing -> error "no setf method"

lookupSetf :: Symbol -> Eval (Maybe LOp)
lookupSetf s = do
    dic <- askEnv setfDef >>= readRef
    return $ lookupDictionary s dic

defineSetf :: Symbol -> LOp -> Eval ()
defineSetf s gf = do
    bind <- askEnv setfDef
    modifyRef bind (addDictionary s gf)

addSetf :: Symbol -> LMethod -> Eval ()
addSetf s m = do
    bind <- askEnv setfDef
    dic <- readRef bind
    case lookupDictionary s dic of
      Just (LGeneric p ms) -> modifyRef bind (setDictionary s (LGeneric p (m:ms)))
      Nothing              -> sigError $ "no generic function: "++show s

addAccessorMethod :: Symbol -> LMethod -> Eval ()
addAccessorMethod s m = do
    liftIOtoEval $ putStrLn $ "add accessor "++show s
    exist <- lookupGeneric s
    unless (isJust exist) $ addGeneric s (LGeneric (1, False) [])
    addMethod s m

addSetfMethod :: Symbol -> LMethod -> Eval ()
addSetfMethod s m = do
    exist <- lookupSetf s
    unless (isJust exist) $ defineSetf s (LGeneric undefined [])
    addSetf s m

-- ----------------------------------------------------------------------------
-- Object Inspection
-- ----------------------------------------------------------------------------

-- todo: loop check

toLList :: [LObj] -> Eval LObj
toLList = toLListWith nilSym

toLListWith :: LObj -> [LObj] -> Eval LObj
toLListWith o [] = return o
toLListWith o (x:xs) = do
    cdr <- toLListWith o xs
    liftM LCons $ newMRef (x,cdr)

fromLList :: LObj -> Eval ([LObj], LObj)
fromLList (LCons p) = do
    (car, cdr) <- readRef p
    (tl, rest) <- fromLList cdr
    return (car:tl, rest)
fromLList other     = return ([], other)

fromProperLList :: String -> LObj -> Eval [LObj]
fromProperLList msg lobj = do
    (r, dot) <- fromLList lobj
    when (dot/=nilSym) $ sigError msg
    return r

isProperList :: LObj -> Eval Bool
isProperList o = do
    (_, rest) <- fromLList o
    return $ rest==nilSym

fromLStr :: LObj -> Eval String
fromLStr (LStr ref) = do
    LArray _ a <- readRef ref
    liftIOtoEval (getElems a) >>= mapM (\ (LChar c) -> return c)
fromLStr _ = sigError "domain error"

toLStr :: String -> Eval LObj
toLStr s = do
    a <- liftIOtoEval (newListArray (0, length s-1) (map LChar s))
    liftM LStr $ newMRef $ LArray [length s] a

showLObj :: LObj -> Eval String
showLObj = showLObj' [] [] []
showLObj' mr mp ma (LRef c r)
    | elem r mr  = return "#loop.."
    | otherwise  = do
        dic <- readRef r
        str <- mapM (\ (s, o) -> liftM ((show s++"=")++) $ showLObj' (r:mr) mp ma o)
                    (listFromDictionary dic)
        return $ foldl (\x y -> x++" "++y) ("#<"++show (lcName c)) str ++" >"
showLObj' mr mp ma cons@(LCons p) = do
    str <- showLCons mr mp ma cons
    return $ "("++str
showLObj' mr mp ma (LMeta c) = return $ "#<class "++show (lcName c)++" "++show (lcAccessor c)++">"
--showLObj' mr mp (LMeta c) = return $ "#<class "++show (lcName c)++">"
showLObj' mr mp ma (LClsr _) = return $ "#<closure>"
showLObj' mr mp ma (LInt i) = return $ show i
showLObj' mr mp ma (LFlt f) = return $ show f
showLObj' mr mp ma (LSym s) = return $ show s
showLObj' mr mp ma (LChar c) = return $ "#\\"++[c]
showLObj' mr mp ma (LStr s) = do
    str <- readRef s >>= liftIOtoEval . getElems . laRef
    return $ "\""++map (\ (LChar c) -> c) str++"\""
    
showLObj' mr mp ma (LArr a)
    | elem a ma  = return "#loop.."
    | otherwise  = do
        arr <- readRef a
        arrElems <- liftIOtoEval $ getElems $ laRef arr
        es <- mapM (showLObj' mr mp (a:ma)) arrElems
        return $ showLArray (laDim arr) es
showLObj' mr mp ma LUnbound = return $ "#unbound"

showLCons mr mp ma (LCons p)
    | elem p mp  = return $ "#loop.."
    | otherwise  = do
        (car, cdr) <- readRef p
        strCar <- showLObj' mr (p:mp) ma car
        strCdr <- showLCons mr (p:mp) ma cdr
        if cdr == nilSym  then return (strCar++strCdr)
                          else return (strCar++" "++strCdr)
showLCons mr mp ma o
    | o == nilSym  = return ")"
    | otherwise    = showLObj' mr mp ma o >>= (\s -> return $ ". " ++ s ++ ")")

showLArray lims es = "#"++show (length lims)++showDim lims es where
    showDim [] _ = ""
    showDim (x:xs) es = showL $ map (showDim xs) $ takeWhile (not . null)
                      $ iterate (drop x) es
    showL [] = "()"
    showL (x:xs) = foldl (\acc s -> acc++" "++s) ("("++x) xs ++ ")"

showMethod :: LMethod -> String
showMethod = show . fst

-- ----------------------------------------------------------------------------
-- Object Reader
-- ----------------------------------------------------------------------------
data Expr = E_Cons Expr Expr
          | E_Int  Integer
          | E_Float Double
          | E_Char Char
          | E_Str  String
          | E_Sym  String
          | E_Nil
          | E_Comma   Expr -- only in parsing
          | E_CommaAt Expr -- only in parsing
          deriving Show

data Token = T_Ident String
           | T_Int   Integer
           | T_Float Double
           | T_Char  Char
           | T_Str   String
           | T_LPar
           | T_RPar
           | T_Dot
           | T_Nil
           | T_Quote
           | T_BackQuote
           | T_Comma
           | T_CommaAt
           | T_NQuote
           | T_NLPar
           | T_NArray Integer
           deriving Show

lexer :: String -> [Token]
lexer [] = []
lexer (';':xs) = lexer $ dropWhile (/='\n') xs
lexer ('(':xs) = T_LPar:lexer xs
lexer (')':xs) = T_RPar:lexer xs
lexer ('.':xs) = T_Dot:lexer xs
lexer ('\'':xs) = T_Quote:lexer xs
lexer ('\"':xs) = let (s, next) = lexString xs in T_Str s:lexer next
lexer ('`':xs) = T_BackQuote:lexer xs
lexer (',':'@':xs) = T_CommaAt:lexer xs
lexer (',':xs) = T_Comma:lexer xs
lexer ('#':[])= error "unexpected EOI"
lexer ('#':x:xs)
    | x=='|'  = lexer $ skipBlockComment xs
    | x=='\'' = T_NQuote:lexer xs
    | x=='\\' = case xs of
                  (c:cs) -> T_Char c:lexer cs
                  []     -> error "unexpected EOI"
    | [x]=="("  = T_NLPar:lexer xs
--    | x=='b'||x=='B'  = lexBinNum xs
--    | x=='o'||x=='O'  = lexOctNum xs
--    | x=='x'||x=='X'  = lexHexNum xs
--    | isDigit x       = lexNArray (x:xs)
    | otherwise       = error "unexpected character near '#'"
lexer (':':xs) = case lexer xs of
                   (T_Ident s:ys) -> T_Ident (':':s):ys
                   _              -> error $ "unexpected character near ':'"
lexer ('&':xs) = case lexer xs of
                   (T_Ident s:ys) -> T_Ident (':':s):ys
                   _              -> error $ "unexpected character near '&'"
lexer ('+':[]) = [T_Ident "+"]
lexer ('+':x:xs)
    | isDigit x   = lexNum [x] xs
    | isSymChar x = error $ "unexpected character near '+'"
    | otherwise   = T_Ident "+":lexer (x:xs)
lexer ('-':[]) = [T_Ident "-"]
lexer ('-':x:xs)
    | isDigit x   = lexNum (x:"-") xs
    | isSymChar x = error $ "unexpected character near '-'"
    | otherwise   = T_Ident "-":lexer (x:xs)
lexer xx@(x:xs)
    | isSpace x = lexer xs
    | isDigit x = lexNum [x] xs
    | isSymStart x = let (tok, next) = span isSymChar xx
                     in if map toUpper tok == "NIL"
                           then T_Nil:lexer next
                           else T_Ident tok:lexer next
    | otherwise = error $ "unexpected character "++show x
isSymChar c = isSymStart c || elem c "0123456789+-"
isSymStart c = isAlpha c || elem c "<>/*=?_!$%[]^{}~"

skipBlockComment [] = error "unexpected EOI"
skipBlockComment ('|':'#':xs) = xs
skipBlockComment (_:xs) = skipBlockComment xs

lexString [] = error "unexpected EOI"
lexString ('\\':[]) = error "unexpected EOI"
lexString ('\\':x:xs) = let (s, next) = lexString xs in (x:s, next)
lexString ('\"':ys) = ([], ys)
lexString (x:xs) = let (s, next) = lexString xs in (x:s, next)

lexNum prev [] = [T_Int (read (reverse prev))]
lexNum prev (x:xs)
    | isDigit x   = lexNum (x:prev) xs
    | x=='.'      = lexFloat (x:prev) xs
    | x=='e' || x=='E'  = lexExp (x:prev) xs
    | isSymChar x = error $ "unexpected character "++show x
    | otherwise   = T_Int (read (reverse prev)):lexer (x:xs)
lexFloat prev [] = error "unexpected EOI"
lexFloat prev (x:xs)
    | isDigit x  = lexFloatNum (x:prev) xs
    | otherwise  = error "unexpected character near '.'"
lexFloatNum prev [] = [T_Float (read (reverse prev))]
lexFloatNum prev (x:xs)
    | isDigit x  = lexFloatNum (x:prev) xs
    | x=='e'||x=='E' = lexExp (x:prev) xs
    | otherwise  = T_Float (read (reverse prev)):lexer (x:xs)
lexExp prev [] = error "unexpected EOI"
lexExp prev (x:xs)
    | isDigit x  = lexExpNum (x:prev) xs
    | otherwise  = error "unexpected character near 'E'"
lexExpNum prev [] = [T_Float (read (reverse prev))]
lexExpNum prev (x:xs)
    | isDigit x   = lexExpNum (x:prev) xs
    | isSymChar x = error $ "unexpected character "++show x
    | otherwise   = T_Float (read (reverse prev)):lexer (x:xs)

parse :: [Token] -> (Expr, [Token])
parse (T_Ident s:xs)      = (E_Sym s, xs)
parse (T_Int i:xs)        = (E_Int i, xs)
parse (T_Float f:xs)      = (E_Float f, xs)
parse (T_Char c:xs)       = (E_Char c, xs)
parse (T_Str s:xs)        = (E_Str s, xs)
parse (T_LPar:T_RPar:xs)  = (E_Nil, xs)
parse (T_LPar:xs)         = parseList xs
parse (T_RPar:xs) = error "unexpected \")\""
parse (T_Dot:xs)  = error "unexpected \".\""
parse (T_Nil:xs)          = (E_Nil, xs)
parse (T_Quote:xs)        = let (e, next) = parse xs
                            in (quoteExpr e , next)
parse (T_BackQuote:xs)    = let (e, next) = parse xs
                            in  (expandQuasiQuote e, next)
parse (T_Comma:xs)        = let (e, next) = parse xs in (E_Comma e, next)
parse (T_CommaAt:xs)      = let (e, next) = parse xs in (E_CommaAt e, next)
parse (T_NQuote:xs)       = let (e, next) = parse xs in (close1Expr "function" e, next)
parse (T_NLPar:xs)        = let (e, next) = parse xs
                            in (close1Expr "function" e, next)
parse (T_NArray n:xs) = error "not yet implemented"
parse [] = error "unexpected EOI"



quoteExpr = close1Expr "quote"
close1Expr s e = E_Cons (E_Sym s) (E_Cons e E_Nil)
close2Expr s e1 e2 = E_Cons (E_Sym s) (E_Cons e1 (E_Cons e2 E_Nil))
expandQuasiQuote e@(E_Sym s) = quoteExpr e
expandQuasiQuote e@(E_Cons a d@(E_Cons _ _))
    | hasCommaAt e  = E_Cons (E_Sym "append") (expandByAppend e)
    | otherwise     = close2Expr "cons" (expandQuasiQuote a) (expandQuasiQuote d)
expandQuasiQuote e@(E_Cons a d)
    | hasCommaAt d  = error "syntax error: quasiquote expansion"
    | hasCommaAt a  = close2Expr "cons" (expandByAppend a) (expandQuasiQuote d)
    | otherwise     = close2Expr "cons" (expandQuasiQuote a) (expandQuasiQuote d)
expandQuasiQuote (E_Comma e) = e
expandQuasiQuote e = e
hasCommaAt (E_Cons (E_CommaAt _) _) = True
hasCommaAt (E_Cons _ d)  = hasCommaAt d
hasCommaAt (E_CommaAt _) = True
hasCommaAt _ = False
expandByAppend (E_Cons a d@(E_Cons _ _)) = E_Cons (expandByAppend a) (expandByAppend d)
expandByAppend (E_Cons a E_Nil) = E_Cons (expandByAppend a) E_Nil
expandByAppend (E_Cons a d) = E_Cons (close2Expr "cons" (expandByAppend a) (expandQuasiQuote d)) E_Nil
expandByAppend (E_Comma e) = (close1Expr "list" e)
expandByAppend (E_CommaAt e) = e
expandByAppend e@(E_Sym s) = quoteExpr (E_Cons e E_Nil)
expandByAppend e = e


pexpr (E_Sym s) = s
pexpr (E_Int i) = show i
pexpr (E_Float f) = show f
pexpr (E_Char c) = "#\\"++[c]
pexpr (E_Str s) = "\""++s++"\""
pexpr (E_Cons a d) = pexprlist ("("++pexpr a) d
pexpr (E_Comma e) = ","++pexpr e
pexpr (E_CommaAt e) = ",@"++pexpr e
pexpr E_Nil = "nil"
pexprlist pr (E_Cons a d) = pexprlist (pr++" "++pexpr a) d
pexprlist pr E_Nil = pr++")"
pexprlist pr e = pr++" . "++pexpr e++")"
pe = pexpr . fst . parse . lexer

parseList (T_RPar:xs) = (E_Nil, xs)
parseList toks = case parse toks of
    (car, T_Dot:xs) -> let (cdr, ys) = parse xs
                       in case ys of
                            (T_RPar:zs) -> (E_Cons car cdr, zs)
                            _           -> error "unexpected token near '.'"
    (car, xx)       -> let (cdr, ys) = parseList xx
                       in (E_Cons car cdr, ys)

readLObjList :: String -> Eval [LObj]
readLObjList s = toLazy $ readLObjList' (lexer s) where
    toLazy e = EvalT $ (mapReaderT $ mapExceptT $ Unsafe.unsafeInterleaveIO) (runEvalT e)

readLObjList' [] = return []
readLObjList' ts = do let (e, next) = parse ts
                      o <- convertLObj e
                      os <- readLObjList' next
                      return (o:os)

readLObj :: String -> Eval LObj
readLObj s = let (e, next) = parse (lexer s)
             in convertLObj e

convertLObj :: Expr -> Eval LObj
convertLObj (E_Sym s) = return (LSym (toSym s))
convertLObj (E_Int i) = return (LInt i)
convertLObj (E_Float f) = return (LFlt f)
convertLObj (E_Char c) = return (LChar c)
convertLObj (E_Str s) = toLStr s
convertLObj (E_Cons a d) = do
    car <- convertLObj a
    cdr <- convertLObj d
    pair <- newMRef (car, cdr)
    return (LCons pair)
convertLObj E_Nil = return nilSym
convertLObj e = trace (pexpr e) (error "reader error")

-- ----------------------------------------------------------------------------
-- Evaluation
-- ----------------------------------------------------------------------------

eval :: LObj -> Eval LObj
eval obj@(LCons _)  = do
    form <- fromProperLList "illegal form" obj
    case head form of
      LSym sym -> lookupLexicalFun sym >>=
                  maybe (sigError $ "unbounded function: "++show sym)
                        (evalApp (tail form))
      LCons _  -> do car <- fromProperLList "illegal form" (head form)
                     case car of
                       LSym s:param:body
                           | s==toSym "lambda" -> makeLambda param body >>=
                                                  evalApp (tail form)
                           | otherwise -> sigError "not symbol"
                       _               -> sigError "not symbol"
--      _        -> showLObj (head form) >>= liftIOtoEval . putStrLn >> sigError "illegal form"
      _        -> mapM showLObj form >>= liftIOtoEval . print >> sigError "illegal form"
eval obj@(LSym sym) = lookupLexicalVar sym >>=
                      maybe (sigError $ "unbounded variable: "++show sym) return
eval LUnbound       = sigError "unbounded object"
eval obj            = return obj

evalApp :: [LObj] -> LOp -> Eval LObj
--evalApp arg (LLambda _ fun) = mapM eval arg >>= fun
evalApp arg (LLambda s fun) = do
    os <- mapM eval arg
    fun os `catchSignal` showFrames s os
evalApp arg (LSpecial fun) = fun arg
evalApp arg (LGeneric _ gfs) = mapM eval arg >>= appGeneric gfs

-- to fix: remove evalApp'
evalApp' arg (LLambda s fun) = do
    fun arg `catchSignal` showFrames s arg
evalApp' arg (LGeneric _ gfs) = appGeneric gfs arg

showFrames s os e@(Sig_Error _) = do
    ss <- mapM showLObj os
    liftIOtoEval $ putStrLn $ "frame: "++show s++" "++show ss
    throwSignal e
showFrames _ _ e = throwSignal e

evalProgn :: [LObj] -> Eval LObj
evalProgn [] = return nilSym
evalProgn fs = liftM last $ mapM eval fs

-- ----------------------------------------------------------------------------
-- Function & Closure
-- ----------------------------------------------------------------------------
makeFunction env name plist restp body =
    LLambda name $ makeCallable env name plist restp body

makeMacro env name plist restp body = LSpecial mac
    where
    expander = makeCallable env name plist restp body
    recError = \_ -> sigError $ "recursive macro "++show name
    mac arg = localFuns [(name, LSpecial recError)]
                        (expander arg >>= eval)


makeCallable :: Env -> Symbol -> [Symbol] -> Maybe Symbol -> [LObj] -> LFun
makeCallable env name plist restp body = maybe fun funN restp where
    fun objs
        | length objs /= length plist  = sigError $ "arity error at "++show name
        | otherwise = invokeFun plist objs
    funN rsym objs
        | length objs < length plist = sigError $ "arity error at "++show name
        | otherwise = do
            let (o,os) = splitAt (length plist) objs
            rest <- toLList os
            invokeFun (rsym:plist) (rest:o)
    invokeFun realPlist objs =
        local (const env) $ localVars (zip realPlist objs) (evalProgn body)


makeLambda :: LObj -> [LObj] -> Eval LOp
makeLambda param body = do
    (must, rest) <- parseParameterList param
    cenv <- askEnv id
    gsym <- genSym
    return $ LLambda gsym (makeCallable cenv (toSym "lambda") must rest body)


parseParameterList :: LObj -> Eval ([Symbol], Maybe Symbol)
parseParameterList param = do
    paramlist <- fromProperLList "parameter list error" param
    unless (all isLSym paramlist) $ sigError ("parameter list error")
    let (must, rest) = break (==toSym ":rest") (map (\ (LSym s) -> s) paramlist)
    case rest of
      []    -> return (must, Nothing)
      [_]   -> sigError $ "parameter list error"
      [_,r] -> return (must, Just r)
      _     -> sigError $ "parameter list error"

parseFunBindList :: LObj -> Eval [(Symbol, [LObj])]
parseFunBindList ls = do
    bindlist <- fromProperLList "syntax error: binding list" ls
    mapM parseBind bindlist

parseBindList :: LObj -> Eval [(Symbol, LObj)]
parseBindList ls = do
    bindlist <- parseFunBindList ls
    unless (all checkBind bindlist) $ sigError "syntax error: binding list"
    return $ map (\ (s, (o:_)) -> (s, o)) bindlist
    where
        checkBind (s, [_]) = True
        checkBind (s, _)   = False

parseBind :: LObj -> Eval (Symbol, [LObj])
parseBind o = do
    ls <- fromProperLList "syntax error: binding list" o
    when (null ls) $ sigError "syntax error: binding list"
    case head ls of
      LSym s -> return (s, tail ls)
      _      -> sigError "syntax error: binding list"

-- ----------------------------------------------------------------------------
-- Class & Object
-- ----------------------------------------------------------------------------
classOf :: LObj -> Eval LClass
classOf (LRef c r) = return c
classOf (LCons _) = findClass (toSym "<cons>")
classOf (LClsr _) = findClass (toSym "<function>")
classOf (LMeta c) = findClass (lcMetaClass c)
classOf (LInt _)  = findClass (toSym "<integer>")
classOf (LFlt _)  = findClass (toSym "<float>")
classOf s@(LSym _)
    | s == nilSym = findClass (toSym "<null>")
    | otherwise   = findClass (toSym "<symbol>")
classOf (LChar _) = findClass (toSym "<character>")
classOf (LStr _)  = findClass (toSym "<string>")
classOf (LArr r)  = do
    LArray lim _ <- readRef r
    case lim of [_] -> findClass (toSym "<general-vector>")
                _   -> findClass (toSym "<general-array*>")

getSuper :: LClass -> Eval [LClass]
getSuper cls = liftM (removeLeftBy (==)) $ collectSuper cls where
    collectSuper c = mapM findClass (lcSuper c) >>=
                     mapM collectSuper >>=
                     return . (c:) . concat

isSubclass :: LClass -> LClass -> Eval Bool
isSubclass c1 c2 = liftM (any (c2 ==)) (getSuper c1)

type Slot = (Symbol, Maybe Symbol, Maybe LObj, [(Symbol, Symbol)])
makeClass :: Symbol -> [Symbol] -> [Slot] -> Bool -> Symbol -> LClass
makeClass name sclist slots abst meta = LClass name sclist slotdef accessor abst meta
--makeClass name sclist slots abst meta = trace (show accessor) $ LClass name sclist slotdef accessor abst meta
    where
        ss = map (\ (x, s, _, xs) -> (x, s, xs)) slots
        initAccessor = listToDictionary $ map (\s -> (s, emptyDictionary))
                           (map toSym [":reader", ":writer", ":accessor", ":boundp"])
        accessor = foldl (\ accs (sname, _, _, opts) -> addMethodTable accs sname opts)
                         initAccessor slots
        addMethodTable table slotname opts =
            foldl (\dic (kw, arg) -> adjustDictionary (addDictionary slotname arg) kw dic)
                  table opts
        slotdef = map (\ (sn, iarg, iform, _) -> (sn, iarg, iform)) slots

makeInstance lc = do
    slotlist <- computeSlotList lc
    dicref <- newMRef $ listToDictionary (map (\ (x, _, _) -> (x, LUnbound)) slotlist)
    return $ LRef lc dicref

initializeInstance :: LObj -> [LObj] -> Eval LObj
initializeInstance (LRef lc dicref) iargs = do
    slotlist <- computeSlotList lc
    let newdic = listToDictionary (map (\ (s, _, _) -> (s, LUnbound)) slotlist)
        slotdic = listToDictionary $ map (\ (s, Just a, _) -> (a, s))
                                         (filter (\ (_, a, _) -> isJust a) slotlist)
--    newMRef newdic >>= showLObj . LRef lc >>= liftIOtoEval . putStrLn
    doInitarg <- parseInitArgList slotdic newdic iargs
    doInitform <- invokeInitformList doInitarg slotlist
    modifyRef dicref (const doInitform)
    return $ LRef lc dicref

parseInitArgList :: Dictionary Symbol -> Dictionary LObj -> [LObj]
                 -> Eval (Dictionary LObj)
parseInitArgList slotdic dic [] = return dic
parseInitArgList slotdic dic (LSym s:v:next) = do
    case lookupDictionary s slotdic of
      Nothing  -> sigError "unknown initarg"
      Just arg -> case lookupDictionary arg dic of
          Just LUnbound -> parseInitArgList slotdic (setDictionary arg v dic) next
          _             -> sigError "initarg list"

invokeInitformList dic slotlist = do
    let idic = listToDictionary $ map (\ (s, _, o) -> (s, o)) slotlist
    foldM (invokeInitform idic) dic (listFromDictionary dic)
invokeInitform idic dic (s, LUnbound) =
    case lookupDictionary s idic of
      Just Nothing  -> return dic
      Just (Just f) -> eval f >>= (\o -> return $ setDictionary s o dic)
      Nothing       -> error "internal"
invokeInitform _ dic _ = return dic


computeSlotList lc = do
    lcs <-getSuper lc
    let allSlotDef = concatMap lcSlots lcs
        usedSlotDef = removeRightBy (\ (x,_,_) (y,_,_) -> x==y) allSlotDef
    return usedSlotDef

parseClassOpt opts = do
    (abst, metac) <- parseClassOpt' Nothing Nothing opts
    return (maybe False id abst, maybe (toSym "<standard-class>") id metac)
parseClassOpt' abst metac [] = return (abst,metac)
parseClassOpt' abst metac (LSym opt:arg:xs)
    | opt==toSym ":abstractp" && isNothing abst = do
        flag <- eval arg
        parseClassOpt' (Just (flag/=nilSym)) metac xs
    | opt==toSym ":metaclass" && isNothing metac = do
        case arg of
          LSym s -> parseClassOpt' abst (Just s) xs
          _      -> sigError "domain error"
    | otherwise = sigError "domain error"

parseSlotList ls = do
    slots <- fromProperLList "syntax error: slot specs" ls
    slotlist <- mapM parseSlotSpec slots
    return slotlist

parseSlotSpec :: LObj -> Eval Slot
parseSlotSpec (LSym s) = return (s, Nothing, Nothing, [])
parseSlotSpec o        = do
    spec <- fromProperLList "syntax error: slot option" o
    case head spec of
      (LSym name) -> parseSlotSpec' name Nothing Nothing [] (tail spec)
      _           -> sigError "syntax error: slot option"
parseSlotSpec' :: Symbol -> Maybe Symbol -> Maybe LObj -> [(Symbol, Symbol)]
               -> [LObj]
               -> Eval (Symbol, Maybe Symbol, Maybe LObj, [(Symbol, Symbol)])
parseSlotSpec' name iarg iform prev [] = return (name, iarg, iform, prev)
parseSlotSpec' name iarg iform prev (LSym opt:arg:next)
    | opt `notElem` validSlotOpt = sigError "syntax error: slot option"
    | opt == toSym ":initarg"    = do
        when (isJust iarg) $ sigError "multiple ':initarg options"
        case arg of
          LSym initarg -> parseSlotSpec' name (Just initarg) iform prev next
          _            -> sigError "syntax error: slot option"
    | opt == toSym ":initform"   = do
        when (isJust iform) $ sigError "multiple ':initform options"
        parseSlotSpec' name iarg (Just arg) prev next
    | otherwise                  = do
        case arg of
          LSym fname -> parseSlotSpec' name iarg iform ((opt, fname):prev) next
          _          -> sigError "syntax error: slot option"

validSlotOpt = map toSym [":reader", ":writer", ":accessor", ":boundp", ":initform", ":initarg"]

-- Generic function & method

type GF = ([LClass], LFun)

applicableMethod :: [LClass] -> [GF] -> Eval [GF]
applicableMethod arg gfs =
    filterM (\ (c,_) -> liftM (all id) (zipWithM isSubclass arg c)) gfs

--appGeneric :: [Method] -> [LObj] -> Eval LObj
appGeneric gfs arg = do
    -- liftIOtoEval $ putStrLn $ concatMap showMethod gfs
    argClass <- mapM classOf arg
    allMethods <- mapM (\ (ss, fn) -> mapM findClass ss >>= \cs -> return (cs, fn)) gfs
    appMethods <- applicableMethod argClass allMethods
    -- liftIOtoEval $ putStrLn $ concatMap (show . lcName) argClass
    -- liftIOtoEval $ putStrLn $ concatMap (concatMap (show . lcName) . fst) allMethods
    -- liftIOtoEval $ putStrLn $ concatMap (concatMap (show . lcName) . fst) appMethods
    methodSeq <- sortMethod argClass appMethods
    if null methodSeq then sigError "no applicable method"
                      else snd (head methodSeq) arg

sortMethod :: [LClass] -> [GF] -> Eval [GF]
sortMethod argClass gfs = isortM gfs where
    isortM [] = return []
    isortM (x:xs) = (isortM xs) >>= insertM x
    insertM x [] = return [x]
    insertM x (y:ys) = do mp <- moreSpecMethod argClass x y
                          if mp then return (x:y:ys)
                                else liftM (x:) (insertM y ys)

moreSpecMethod :: [LClass] -> GF -> GF -> Eval Bool
moreSpecMethod mcs (cs1,_) (cs2,_) = cmp mcs cs1 cs2 where
    cmp [] [] [] = sigError "same method"
    cmp (m:ms) (x:xs) (y:ys) = do
        suplist <- getSuper m
        case ahead suplist x y of
          EQ -> cmp ms xs ys
          LT -> return True
          GT -> return False
    ahead [] _ _ = error "internal error: method invocation"
    ahead (c:cs) c1 c2
        | c==c1 && c==c2  = EQ
        | c==c1           = LT
        | c==c2           = GT
        | otherwise       = ahead cs c1 c2


parseParameterProfile :: LObj -> Eval ([(Symbol,Symbol)], Maybe Symbol)
parseParameterProfile param = do
    paramlist <- fromProperLList "parameter profile error" param
    let (must, rest) = break (== LSym (toSym ":rest")) paramlist
    must' <- mapM parseProfile must
    case rest of
      []         -> return (must', Nothing)
      [_]        -> sigError $ "parameter list error"
      [_,LSym r] -> return (must', Just r)
      _          -> sigError $ "parameter list error"
parseProfile (LSym s) = return (s, toSym "<object>")
parseProfile o = do
    p <- fromProperLList "parameter profile error" o
    case p of
      [LSym s, LSym c] -> return (s, c)
      _                -> sigError "parameter profile error"

makeMethod :: Symbol -> [LObj] -> Eval LMethod
makeMethod name (LSym qualifier:prof:body) = makeMethod' name prof body
makeMethod name (prof:body) = makeMethod' name prof body
makeMethod' name prof body = do
    (must, rest) <- parseParameterProfile prof
    e <- askEnv id
    return (map snd must, (makeCallable e name (map fst must) rest body))

makeGeneric :: Symbol -> [LObj] -> Eval LOp
makeGeneric name (param:opts) = do
    (must, rest) <- parseParameterList param -- TODO
    return (LGeneric (length must, isJust rest) [])

-- ----------------------------------------------------------------------------
-- Primitives
-- ----------------------------------------------------------------------------

makePrim name = makePrim' (LLambda (toSym name)) (==) name
makeSpec name = makePrim' LSpecial (==) name
makePrimN name = makePrim' (LLambda (toSym name)) (>=) name
makeSpecN name = makePrim' LSpecial (>=) name

makePrim' :: (LFun -> LOp) -> (Int -> Int -> Bool) -> String -> LFun -> Int
          -> (Symbol, LOp)
makePrim' con cmp name f arity = (toSym name, con op) where
    op args
       | length args `cmp` arity  = f args
       | otherwise                = sigError $ "arity error at "++show name

makeSetf name f arity = (toSym name, (xarg, op))
    where
    xarg = take arity (repeat (toSym "<object>"))
    op args
        | length args == arity = f args
        | otherwise            = sigError $ "arity error at setf"

makeBuiltinClass :: String -> [String] -> Bool -> (Symbol, LClass)
makeBuiltinClass name sup abst =
    (toSym name, makeClass (toSym name) (map toSym sup) [] abst (toSym "<builtin-class>"))
makePrimClass :: String -> [String] -> [String] -> Bool -> (Symbol, LClass)
makePrimClass name sup slotlist abst = 
    (toSym name, makeClass (toSym name) (map toSym sup) slots abst (toSym "<standard-class>"))
    where
        slots = map toSlot slotlist
        toSlot :: String -> Slot
        toSlot s = (toSym s, Just (toSym s), Just nilSym, [(toSym ":reader", toSym s)])
-- type Slot = (Symbol, Maybe Symbol, Maybe LObj, [(Symbol, Symbol)])


-- -------- Functions --------

l_functionp [LClsr _] = return tSym
l_functionp _ = return nilSym

l_function [LSym s] = do
    fn <- lookupLexicalFun s
    case fn of
      Nothing           -> sigError "unbound function"
      Just (LSpecial _) -> sigError "#'special function"
--      Just fn'          -> return $ LClsr fn'
      Just (LLambda _ f)-> return $ LClsr (LLambda s f)
l_function _ = sigError "domain error"

l_lambda (param:body) = liftM LClsr $ makeLambda param body

l_labels (lf:body) = do
    defs <- parseFunBindList lf
    localFuns (zip (map fst defs) (repeat undefined)) $ do
        mapM_ rebind defs
        evalProgn body
    where
        rebind (name, (params:body)) = do
            (must, rest) <- parseParameterList params
            cenv <- askEnv id
            updateLexicalFun name (makeFunction cenv name must rest body)

l_flet (lf:body) = do
    defs <- parseFunBindList lf
    cenv <- askEnv id
    funs <- mapM (make cenv) defs
    localFuns (zip (map fst defs) funs) $ evalProgn body
    where
        make env (name, (params:body)) = do
            (must, rest) <- parseParameterList params
            return $ makeFunction env name must rest body

l_apply (LClsr fn:args) = do
    arglist <- procArgs args
    evalApp' arglist fn
    where
        procArgs [l] = fromProperLList "domain error: apply" l
        procArgs (o:os) = liftM (o:) $ procArgs os
l_apply _ = sigError "domain error: apply"

l_funcall (LClsr fn:args) = evalApp' args fn
l_funcall _ = sigError "domain error: funcall"

-- -------- Defining operators --------

l_defglobal [LSym name, f] = eval f >>= addGlobalVar name >> return (LSym name)
l_defglobal _ = sigError "syntax error at \"defglobal\""

l_defdynamic [LSym name, f] = eval f >>= addDynamicVar name >> return (LSym name)
l_defdynamic _ = sigError "syntax error at \"defdynamic\""

l_defun (LSym name:param:body) = do
    (must, rest) <- parseParameterList param
    cenv <- askEnv id
    addGlobalFun name (makeFunction cenv name must rest body)
    return (LSym name)
l_defun _ = sigError "syntax error at \"defun\""

-- -------- Predicates --------

l_eql [x, y] = return $ toLBool (x==y)

-- -------- Control structure --------

l_quote [o] = return o

l_setq [LSym v, o] = eval o >>= (\o' -> updateLexicalVar v o' >> return o')
l_setq _ = sigError "syntax error: setq"

-- to fix: macro expansion
l_setf [LSym v, o] = eval o >>= (\o' -> updateLexicalVar v o' >> return o')
l_setf [place, o] = do
    pform <- fromProperLList "syntax error: setf" place
    case pform of
      []  -> sigError "syntax error: setf"
      [_] -> sigError "syntax error: setf"
      (LSym pname:os) -> do
          findSetf pname >>= evalApp (os++[o])
      _   -> sigError "syntax error: setf"

l_let (binds:body) = do
    bindList <- parseBindList binds
    bindList' <- mapM (\ (x, f) -> eval f >>= \o -> return (x,o)) bindList
    localVars bindList' (evalProgn body)


l_dynamic [LSym s] = do
    find <- lookupDynamicVar s
    case find of
      Just o  -> return o
      Nothing -> sigError "no dynamic variable s"
l_dynamic _ = sigError "dynamic"

l_set_dynamic [LSym s, o] = do
    o' <- eval o
    updateDynamicVar s o'
    return o'
l_set_dynamic _ = sigError "domain error: set-dynamic"

-- todo rename parseBindList
l_dynamic_let (binds:body) = do
    bindList <- parseBindList binds
    bindList' <- mapM (\ (x, f) -> eval f >>= \o -> return (x,o)) bindList
    localDynamicVar bindList' (evalProgn body)


l_if [pred, thenc] = do
    p <- eval pred
    if p/=nilSym then eval thenc else return nilSym
l_if [pred, thenc, elsec] = do
    p <- eval pred
    if p/=nilSym then eval thenc else eval elsec
l_if _ = sigError "arity error: if"

l_progn = evalProgn

l_block (LSym name:forms) = do
    uid <- liftM LSym genSym
    localBlock name (exit uid) (evalProgn forms) `catchSignal` (catcher uid)
    where
        exit tag o = throwSignal (Sig_Throw tag o)
        catcher tag e@(Sig_Throw t o)
            | tag == t  = return o
            | otherwise = throwSignal e
        catcher tag e = throwSignal e

l_return_from [LSym name, o] = do
    exit <- getBlockExit name
    r <- eval o
    case exit of
      Just fn -> fn r
      Nothing -> sigError "no block name"
l_return_from _ = sigError "domain error"

l_tagbody os = do
    uid <- genSym
    tags <- parseTagbody os
    let forms = if null tags then [] else snd (head tags)
        exits = map (makeExit uid) tags
    localTagbody exits (evalProgn forms) `catchSignal` (catcher uid)
    return nilSym
    where
        catcher uid e@(Sig_TagGo t next)
            | uid == t  = evalProgn next
            | otherwise = throwSignal e
        catcher uid e = throwSignal e
        makeExit uid (tag,forms) = (tag, throwSignal (Sig_TagGo uid forms))

l_go [LSym tag] = do
    go <- getTagbodyGo tag
    case go of
      Just fn -> fn
      Nothing -> sigError "no tag name"
    return nilSym -- not come here
l_go _ = sigError "domain error"

parseTagbody :: [LObj] -> Eval [(Symbol, [LObj])]
parseTagbody [] = return []
parseTagbody (LSym t:form:next) = do
    nextBody <- parseTagbody next
    case nextBody of
      []          -> return [(t, [form])]
      ((_,fs):_)  -> return $ (t, form:fs):nextBody
parseTagbody _ = sigError "syntax error: tagbody"

l_throw [t, o] = throwSignal $ Sig_Throw t o
l_catch (t:fs) = evalProgn fs `catchSignal` (catcher t) where
    catcher t e@(Sig_Throw tag o)
        | t == tag  = return o
        | otherwise = throwSignal e

l_unwind_protect (f:cfs) = unwindProtect (eval f) (evalProgn cfs) 


-- Class & Object & Generic function

l_defclass (LSym name:sup:slots:opts) = do
    (suplist, dot) <- fromLList sup
    unless (dot==nilSym) $ sigError "domain error: defclass"
    unless (all isLSym suplist) $ sigError "domain error: defclass"
    slots <- parseSlotList slots
    (abst, metac) <- parseClassOpt opts
    let suplist' = map (\ (LSym s) -> s) suplist
        suplist'' = if null suplist' then [toSym "<standard-object>"] else suplist'
        nclass = makeClass name suplist'' slots abst metac
    addClassDef name nclass
    addSlotMethods name (listFromDictionary $ lcAccessor nclass)
    return (LSym name)


addSlotMethods cname = mapM_ (addSlotMethod cname)
addSlotMethod cname (sym, dic)
    | sym==toSym ":reader"  = mapM_ (addSlotReader cname) (listFromDictionary dic)
    | sym==toSym ":writer"  = mapM_ (addSlotWriter cname) (listFromDictionary dic)
    | sym==toSym ":accessor" = mapM_ (addSlotAccessor cname) (listFromDictionary dic)
    | sym==toSym ":boundp"  = mapM_ (addSlotBoundp cname) (listFromDictionary dic)
    | sym==toSym ":initarg" = return ()
    | otherwise = error $ "internal "++show sym

addSlotReader cname (slot, fname) = addAccessorMethod fname ([cname], getter) where
    getter [o] = getSlot slot o
    getter _   = sigError "arity error"
addSlotWriter cname (slot, fname) = addAccessorMethod fname ([cname], setter) where
    setter [o,v] = setSlot slot v o >> return v
    setter _     = sigError "arity error"
addSlotAccessor cname (slot, fname) = do
    addSlotReader cname (slot, fname)
    addSetfMethod fname ([cname], setter)
    where
    setter [o,v] = setSlot slot v o >> return v
    setter _     = sigError "arity error"
addSlotBoundp cname (slot, fname) = addAccessorMethod fname ([cname], boundp) where
    boundp [o] = return $ toLBool (o==LUnbound)
    boundp _   = sigError "arity error"


l_defmethod (LSym name:o) = makeMethod name o >>= addMethod name >> return (LSym name)
l_defmethod (p:o) = do
    ss <- fromProperLList "syntax error: defemthod" p
    case ss of
      [LSym setf, LSym sname]
          | setf==toSym "setf" -> makeMethod sname o >>= addSetf sname >> return (LSym sname)
          | otherwise          -> sigError "syntax error: defmethod"
      _                        -> sigError "syntax error: defmethod"

l_defgeneric (LSym name:o) = makeGeneric name o >>= addGeneric name >> return (LSym name)
l_defgeneric (p:o) = do
    ss <- fromProperLList "syntax error: defgeneric" p
    case ss of
      [LSym setf, LSym name]
          | setf==toSym "setf" -> makeGeneric name o >>= defineSetf name >> return (LSym name)
          | otherwise          -> sigError "syntax error: defgeneric"
      _                        -> sigError "syntax error: defgeneric"


l_generic_function_p [LClsr (LGeneric _ _)] = return tSym
l_generic_function_p _ = return nilSym

l_class [LSym name] = liftM LMeta $ findClass name
l_class _ = sigError "syntax error: class"

l_create (LMeta lc:iargs) = do
    when (lcName lc==toSym "<builtin-class>") $
        sigError "cannot create <builtin-class>"
    newobj <- makeInstance lc
    initializeInstance newobj iargs
l_create _ = sigError "domain error"

l_initialize_object [o, iarg] = do
    lc <- classOf o
    when (lcName lc==toSym "<builtin-class>") $
        sigError "cannot initialize instance of <builtin-class>"
    ilist <- fromProperLList "invalid argument: initialize_object" iarg
    initializeInstance o ilist

l_subclassp [LMeta c1, LMeta c2] = do
    r <- isSubclass c1 c2
    return $ toLBool r
l_subclassp [_, _] = sigError "domain error: subclassp"

l_defmacro (LSym name:param:body) = do
    (must, rest) <- parseParameterList param
    cenv <- askEnv id
    addGlobalFun name (makeMacro cenv name must rest body)
    return (LSym name)
l_defmacro _ = sigError "syntax error at \"defmacro\""

l_symbolp [s] = return $ toLBool (isLSym s)

l_gensym [] = liftM LSym genSym

-- -------- Number --------

l_numeq [o1, o2] = binCmp (==) (==) o1 o2
l_numgt [o1, o2] = binCmp (>) (>) o1 o2
l_numlt [o1, o2] = binCmp (<) (<) o1 o2

binCmp o p (LInt i) (LInt j) = return $ toLBool (i `o` j)
binCmp o p (LInt i) (LFlt f) = return $ toLBool (fromInteger i `p` f)
binCmp o p (LFlt f) (LInt i) = return $ toLBool (f `p` fromInteger i)
binCmp o p (LFlt f) (LFlt g) = return $ toLBool (f `p` g)
binCmp o p _ _ = sigError "domain error"

l_add (o:os) = foldM (binOp (+) (+)) o os
l_mul (o:os) = foldM (binOp (*) (*)) o os

l_sub [LFlt x] = return $ LFlt (-x)
l_sub [LInt x] = return $ LInt (-x)
l_sub (o:os) = foldM (binOp (-) (-)) o os

l_quotient (o:os) = foldM binQuotient o os
binQuotient (LInt i) (LInt j)
    | mod i j == 0  = return $ LInt (i `div` j)
    | otherwise     = return $ LFlt (fromInteger i / fromInteger j)
binQuotient (LInt i) (LFlt f) = return $ LFlt (fromInteger i / f)
binQuotient (LFlt f) (LInt i) = return $ LFlt (f / fromInteger i)
binQuotient (LFlt g) (LFlt f) = return $ LFlt (f / g)

l_log [o] = fltOp log o
l_exp [o] = fltOp exp o
l_expt [o1, o2] = binOp (^) (**) o1 o2
l_sqrt [o] = fltOp sqrt o
l_sin [o] = fltOp sin o
l_cos [o] = fltOp cos o
l_tan [o] = fltOp tan o
l_atan [o] = fltOp atan o
l_atan2 [o1, o2] = fltBinOp atan2 o1 o2
l_sinh [o] = fltOp sinh o
l_cosh [o] = fltOp cosh o
l_tanh [o] = fltOp tanh o
l_atanh [o] = fltOp atanh o

binOp o p (LInt i) (LInt j) = return $ LInt (i `o` j)
binOp o p (LInt i) (LFlt f) = return $ LFlt (fromInteger i `p` f)
binOp o p (LFlt f) (LInt i) = return $ LFlt (f `p` fromInteger i)
binOp o p (LFlt f) (LFlt g) = return $ LFlt (f `p` g)
binOp o p _ _ = sigError "domain error"

fltOp op (LInt i) = return $ LFlt $ op (fromInteger i)
fltOp op (LFlt f) = return $ LFlt $ op f
fltOp op _ = sigError "domain error"

fltBinOp op (LInt i) (LInt j) = return $ LFlt (fromInteger i `op` fromInteger j)
fltBinOp op (LInt i) (LFlt f) = return $ LFlt (fromInteger i `op` f)
fltBinOp op (LFlt f) (LInt i) = return $ LFlt (f `op` fromInteger i)
fltBinOp op (LFlt f) (LFlt g) = return $ LFlt (f `op` g)
fltBinOp op _ _ = sigError "domain error"

l_floatp [LFlt f] = return tSym
l_floatp _ = return nilSym

l_integerp [LInt i] = return tSym
l_integerp _ = return nilSym

l_div [LInt i, LInt j] = return $ LInt (div i j)
l_div _ = sigError "domain error"

l_mod [LInt i, LInt j] = return $ LInt (mod i j)
l_mod _ = sigError "domain error"


l_characterp [LChar _] = return tSym
l_characterp _ = return nilSym

l_chareq = charCmp (==)
l_charlt = charCmp (<)
l_chargt = charCmp (>)

charCmp op [LChar c1, LChar c2] = return $ toLBool (c1 `op` c2)
charCmp _ _ = sigError "domain error"

-- -------- List --------
l_consp [LCons _] = return tSym
l_consp _ = return nilSym

l_cons [a,d] = liftM LCons $ newMRef (a, d)

l_car [LCons p] = liftM fst $ readRef p
l_car [_]       = sigError "car: domain error"

l_cdr [LCons p] = liftM snd $ readRef p
l_cdr [_]       = sigError "cdr: domain error"

l_setcar [LCons p, o] = modifyRef p (\ (a, d) -> (o, d)) >> return o
l_setcar _ = sigError "set-car: domain error"
l_setcdr [LCons p, o] = modifyRef p (\ (a, d) -> (a, o)) >> return o
l_setcdr o = sigError "set-cdr: domain error"

l_list = toLList

l_append :: [LObj] -> Eval LObj
l_append [] = return nilSym
l_append os = foldrM concatLList (nilSym, []) os >>= uncurry toLListWith where
    concatLList cons (end, acc) = do
        (l, dot) <- fromLList cons
        when (end/=nilSym && dot/=nilSym) $ sigError "domain error: append"
        return (dot, l++acc)

-- -------- Array --------

l_basic_array_p [LArr _] = return tSym
l_basic_array_p [LStr _] = return tSym
l_basic_array_p _ = return nilSym

l_basic_marray_p = l_general_array_p

l_general_array_p [LArr a] = do
    LArray lim _ <- readRef a
    case lim of [_] -> return nilSym
                _   -> return tSym
l_general_array_p _ = return nilSym

l_garef (LArr ref:zs) = do
    a <- readRef ref
    unless (all isLInt zs) $ sigError "domain error: aref"
    let zs' = map (\ (LInt i) -> fromInteger i) zs
    readLArray a zs'
l_garef _ = sigError "domain error: garef"

l_set_garef (o:LArr ref:zs) = do
    a <- readRef ref
    unless (all isLInt zs) $ sigError "domain error: aref"
    let zs' = map (\ (LInt i) -> fromInteger i) zs
    writeLArray a zs' o
    return o
l_set_garef _ = sigError "domain error: set-garef"

l_array_dimensions [LArr ref] = do
    LArray lim _ <- readRef ref
    toLList (map (\i -> LInt (toInteger i)) lim)
l_array_dimensions _ = sigError "domain error: array-dimensions"

readLArray :: LArray -> [Int] -> Eval LObj
readLArray (LArray lim a) ix = do
    unless (length lim /= length ix) $ sigError "dimension error"
    unless (all (0 <=) ix) $ sigError "index out of range"
    unless (all id (zipWith (<) ix lim)) $ sigError "index out of range"
    o <- liftIOtoEval $ readArray a (getLArrayIx lim ix)
    return o

writeLArray :: LArray -> [Int] -> LObj -> Eval ()
writeLArray (LArray lim a) ix o = do
    unless (length lim /= length ix) $ sigError "dimension error"
    unless (all (0 <=) ix) $ sigError "index out of range"
    unless (all id (zipWith (<) ix lim)) $ sigError "index out of range"
    liftIOtoEval $ writeArray a (getLArrayIx lim ix) o

getLArrayIx :: [Int] -> [Int] -> Int
getLArrayIx lims zs = foldl (\ r (lim, z) -> r*lim + z) 0 (zip lims zs)

l_basic_vector_p [LStr _] = return tSym
l_basic_vector_p os = l_general_vector_p os

l_general_vector_p [LArr a] = do
    LArray lim _ <- readRef a
    case lim of
      [_] -> return tSym
      _   -> return nilSym

-- -------- Sequence --------

l_length [LStr s] = do
    LArray [len] _ <- readRef s
    return $ LInt (toInteger len)
l_length [LArr a] = do
    LArray lim _ <- readRef a
    case lim of
      [len] -> return $ LInt (toInteger len)
      _     -> sigError "domain error"
l_length _ = sigError "domain error"

l_elt [LStr ref, LInt i] = do
    a <- readRef ref
    readLArray a [fromInteger i]
l_elt [LArr ref, LInt i] = do
    a@(LArray lim _) <- readRef ref
    case lim of
      [len] -> readLArray a [fromInteger i]
      _     -> sigError "domain error"
l_elt [p@(LCons _), LInt i] = do
    (list, _) <- fromLList p
    if genericLength list <= i then sigError "index out of range"
                               else return $ list !! (fromInteger i)
l_elt _ = sigError "domain error"

l_set_elt [LStr ref, LInt i, o] = do
    a <- readRef ref
    writeLArray a [fromInteger i] o
    return o
l_set_elt [LArr ref, LInt i, o] = do
    a@(LArray lim _) <- readRef ref
    case lim of
      [len] -> writeLArray a [fromInteger i] o >> return o
      _     -> sigError "domain error"
l_set_elt [p@(LCons _), LInt i, o]
    | i < 0     = sigError "index out of range"
    | otherwise = setEltList p i >> return o
    where
        setEltList (LCons p) 0 = modifyRef p (\ (_, y) -> (o, y))
        setEltList (LCons p) n = do
            (_, cdr) <- readRef p
            setEltList cdr (n-1)
        setEltList _ _ = sigError "index out of range"
l_set_elt _ = sigError "domain error"

-- -------- Stream --------

l_streamp [LIOS _] = return tSym
l_streamp [LStrS _] = return tSym
l_streamp _ = return nilSym

l_open_stream_p [LIOS h] = liftM toLBool $ liftIOtoEval $ hIsOpen h
l_open_stream_p [LStrS r] = readRef r >>= (\ (p, _, _) -> return $ toLBool p)
l_open_stream_p _ = sigError "domain error"

l_input_stream_p [LIOS h] = liftM toLBool $ liftIOtoEval $ hIsReadable h
l_input_stream_p [LStrS r] = readRef r >>= (\ (_, i, _) -> return $ toLBool i)
l_input_stream_p _ = sigError "domain error"

l_output_stream_p [LIOS h] = liftM toLBool $ liftIOtoEval $ hIsWritable h
l_output_stream_p [LStrS r] = readRef r >>= (\ (_, i, _) -> return $ toLBool (not i))
l_output_stream_p _ = sigError "domain error"

l_standard_input [] = findDynamicVar (toSym "*standard-input*")
l_standard_output [] = findDynamicVar (toSym "*standard-output*")
l_standard_error [] = findDynamicVar (toSym "*standard-error*")

l_open_input_file [fname] = fromLStr fname >>= lOpenFile False ReadMode
l_open_input_file [fname, LInt i]
    | i == 8    = fromLStr fname >>= lOpenFile True ReadMode
    | otherwise = sigError "not implemented"
l_open_input_file _ = sigError "domain error"

l_open_output_file [fname] = fromLStr fname >>= lOpenFile False WriteMode
l_open_output_file [fname, LInt i]
    | i == 8    = fromLStr fname >>= lOpenFile True WriteMode
    | otherwise = sigError "not implemented"
l_open_output_file _ = sigError "domain error"

l_open_io_file [fname] = fromLStr fname >>= lOpenFile False ReadWriteMode
l_open_io_file [fname, LInt i]
    | i == 8    = fromLStr fname >>= lOpenFile True ReadWriteMode
    | otherwise = sigError "not implemented"
l_open_io_file _ = sigError "domain error"

lOpenFile True rm fn = liftIOtoEval (openBinaryFile fn rm) >>= return . LIOS
lOpenFile False rm fn = liftIOtoEval (openFile fn rm) >>= return . LIOS

l_close [LIOS h] = liftIOtoEval (hClose h) >> return nilSym
l_close [LStrS r] = modifyRef r (\ (_, i, n) -> (False, i, n)) >> return nilSym
l_close _ = sigError "domain error"

l_finish_output [LIOS h] = liftIOtoEval (hFlush h) >> return nilSym
l_finish_output _ = sigError "domain error"

-- -------- Condition -------

l_signal_condition [e] = sigCondition e

l_with_handler (h:forms) = do
    ref <- askEnv errHandler
    h' <- eval h
    case h' of
      LClsr f -> modifyRef ref (makeHandler f:) >>
                 unwindProtect (evalProgn forms) (modifyRef ref tail)
      _       -> sigError "domain error"
    where
        makeHandler f arg = evalApp [arg] f

-- -------- Other -------

l_load [e] = do
    fname <- fromLStr e
    str <- liftIOtoEval $ readFile fname
    src <- readLObjList str
    mapM_ (\e -> eval e >>= showLObj >>= liftIOtoEval . putStrLn) src
    return tSym

-- --------  -------

primFun =
    [ makePrim "functionp" l_functionp 1
    , makeSpec "function" l_function 1
    , makeSpecN "lambda" l_lambda 1
    , makeSpecN "labels" l_labels 1
    , makeSpecN "flet" l_flet 1
    , makePrimN "apply" l_apply 2
    , makePrimN "funcall" l_funcall 1
    , makeSpec "defglobal" l_defglobal 2
    , makeSpec "defdynamic" l_defdynamic 2
    , makeSpecN "defun" l_defun 2
    , makePrim "eql" l_eql 2
    , makeSpec "quote" l_quote 1
    , makeSpec "setq" l_setq 2
    , makeSpec "setf" l_setf 2
    , makeSpecN "let" l_let 1
    , makeSpec "dynamic" l_dynamic 1
    , makeSpec "set-dynamic" l_set_dynamic 2
    , makeSpecN "dynamic-let" l_dynamic_let 1
    , makeSpecN "if" l_if 2
    , makeSpecN "progn" l_progn 0
    , makeSpecN "block" l_block 1
    , makeSpec "return-from" l_return_from 2
    , makeSpecN "catch" l_catch 1
    , makePrim "throw" l_throw 2
    , makeSpecN "tagbody" l_tagbody 0
    , makeSpec "go" l_go 1
    , makeSpecN "unwind-protect" l_unwind_protect 1
    , makeSpecN "defclass" l_defclass 3
    , makeSpecN "defgeneric" l_defgeneric 2
    , makeSpecN "defmethod" l_defmethod 2
    , makePrim "generic-function-p" l_generic_function_p 1
    , makeSpec "class" l_class 1
    , makePrimN "create" l_create 1
    , makePrim "subclassp" l_subclassp 2
    , makeSpecN "defmacro" l_defmacro 2
    , makePrim "symbolp" l_symbolp 1
    , makePrim "gensym" l_gensym 0
    , makePrim "=" l_numeq 2
    , makePrim ">" l_numgt 2
    , makePrim "<" l_numlt 2
    , makePrimN "+" l_add 1
    , makePrimN "*" l_mul 1
    , makePrimN "-" l_sub 1
    , makePrimN "quotient" l_quotient 2
    , makePrim "log" l_log 1
    , makePrim "exp" l_exp 1
    , makePrim "expt" l_expt 1
    , makePrim "sqrt" l_sqrt 1
    , makePrim "sin" l_sin 1
    , makePrim "cos" l_cos 1
    , makePrim "tan" l_tan 1
    , makePrim "atan" l_atan 1
    , makePrim "atan2" l_atan2 2
    , makePrim "sinh" l_sinh 1
    , makePrim "cosh" l_cosh 1
    , makePrim "tanh" l_tanh 1
    , makePrim "atanh" l_atanh 1
    , makePrim "floatp" l_floatp 1
    , makePrim "integerp" l_integerp 1
    , makePrim "div" l_div 2
    , makePrim "mod" l_mod 2
    , makePrim "characterp" l_characterp 1
    , makePrim "char=" l_chareq 2
    , makePrim "char<" l_charlt 2
    , makePrim "char>" l_chargt 2
    , makePrim "consp" l_consp 1
    , makePrim "cons" l_cons 2
    , makePrim "car" l_car 1
    , makePrim "cdr" l_cdr 1
    , makeSpec "set-car" l_setcar 2
    , makeSpec "set-cdr" l_setcdr 2
    , makePrimN "list" l_list 0
    , makePrimN "append" l_append 0
    , makePrim "basic-array-p" l_basic_array_p 1
    , makePrim "basic-array*-p" l_basic_marray_p 1
    , makePrim "general-array*-p" l_general_array_p 1
    , makePrimN "garef" l_garef 2
    , makePrimN "set-garef" l_set_garef 3
    , makePrim "array-dimensions" l_array_dimensions 1
    , makePrim "basic-vector-p" l_basic_vector_p 1
    , makePrim "general-vector-p" l_general_vector_p 1
    , makePrim "length" l_length 1
    , makePrim "elt" l_elt 2
    , makePrim "set-elt" l_set_elt 3
    ]

primFunExt =
    [ makePrim "load" l_load 1
    ]

primVar = [ (toSym "t", tSym), (toSym "nil", nilSym) ]

primSetf = [ makeSetf "car" l_setcar 2
           , makeSetf "cdr" l_setcdr 2
           ]

primClass =
    [ makeBuiltinClass "<object>" [] True
    , makeBuiltinClass "<builtin-class>" ["<object>"] True
    , makeBuiltinClass "<standard-class>" ["<object>"] True
    , makeBuiltinClass "<standard-object>" ["<object>"] True
    , makeBuiltinClass "<function>" ["<object>"] False
    , makeBuiltinClass "<generic-function>" ["<object>"] False
    , makeBuiltinClass "<symbol>" ["<object>"] False
    , makeBuiltinClass "<list>" ["<object>"] True
    , makeBuiltinClass "<cons>" ["<list>"] False
    , makeBuiltinClass "<null>" ["<symbol>", "<null>"] False
    , makeBuiltinClass "<character>" ["<object>"] False
    , makeBuiltinClass "<basic-array>" ["<object>"] True
    , makeBuiltinClass "<basic-array*>" ["<basic-array>"] True
    , makeBuiltinClass "<general-array*>" ["<basic-array*>"] False
    , makeBuiltinClass "<basic-vector>" ["<basic-array>"] True
    , makeBuiltinClass "<general-vector>" ["<basic-vector>"] False
    , makeBuiltinClass "<string>" ["<basic-vector>"] False
    , makeBuiltinClass "<number>" ["<object>"] True
    , makeBuiltinClass "<float>" ["<number>"] False
    , makeBuiltinClass "<integer>" ["<number>"] False
    , makeBuiltinClass "<serious-condition>" ["<object>"] True
    , makePrimClass "<error>" ["<serious-condition>"] ["format-string", "format-arguments"] True
    , makePrimClass "<arithmetic-error>" ["<error>"] [] True
    , makePrimClass "<division-by-zero>" ["<arithmetic-error>"] [] False
    , makePrimClass "<control-error>" ["<error>"] [] False
    , makePrimClass "<parse-error>" ["<error>"] [] False
    , makePrimClass "<program-error>" ["<error>"] [] False
    , makePrimClass "<domain-error>" ["<program-error>"] [] False
    , makePrimClass "<undefined-entity>" ["<program-error>"] [] True
    , makePrimClass "<unbound-variable>" ["<undefined-entity>"] [] False
    , makePrimClass "<undefined-function>" ["<undefined-entity>"] [] False
    , makePrimClass "<simple-error>" ["<error>"] [] False
    ]

initStream = do
    addDynamicVar (toSym "*standard-input*") (LIOS stdin)
    addDynamicVar (toSym "*standard-output*") (LIOS stdout)
    addDynamicVar (toSym "*standard-error*") (LIOS stderr)

initEnv = do
    mapM_ (uncurry addClassDef) primClass
    mapM_ (uncurry addGlobalFun) primFun
    mapM_ (uncurry addGlobalFun) primFunExt
    mapM_ (uncurry addGlobalVar) primVar
    mapM_ (uncurry addSetfMethod) primSetf
    initStream
    -- h <- liftIOtoEval $ openFile "init.lisp" ReadMode
    -- str <- liftIOtoEval $ hGetContents h
    let str = initLisp
    src <- readLObjList str
    mapM_ eval src

runRepl = runEval $ do
    initEnv
    catchSignal repl (\e -> liftIOtoEval (print e) >> return ())

repl = do
    input <- liftIOtoEval getLine >>= readLObjList
    mapM_ (\o -> eval o >>= showLObj >>= liftIOtoEval . putStrLn) input
    repl

{-
runTest = runEval $ do
    initEnv
    h <- liftIOtoEval $ openFile "test.lisp" ReadMode
    str <- liftIOtoEval $ hGetContents h
    src <- readLObjList str
    return src
    mapM_ (\e -> eval e >>= showLObj >>= liftIOtoEval . putS) src
    where
        putS x = putStr (x++" ")

test s = runEval $ do
    initEnv
    src <- readLObjList s
    catchSignal (mapM_ (\o -> eval o >>= showLObj >>= liftIOtoEval . putStrLn) src)
                (\e -> liftIOtoEval (print e) >> return ())



testStr = "(defglobal x 2) (defun f(x) (setf x 1) x) (f 2) x"
main = runRepl >> return ()
-}

initLisp = $(compileTimeReadFile "init.lisp")
